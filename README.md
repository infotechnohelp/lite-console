# Install 

Be sure your project's `composer.json` includes:

```json
{
  "minimum-stability": "dev",
  "config": {
    "bin-dir": "bin"
  }
}
```

`composer require infotechnohelp/lite-console`

This will create a `bin/lite` (composer symlink) file inside your project.


# Configure Lite Console

Create `/lite-console.json` file inside your project's root

`namespace` - required

`naming exceptions` - optional

If directory with LiteConsole command files is inside `src`

```json
{
  "namespace": "App\\LiteConsole\\"
}
```

If composer autoload psr-4 namespace `LiteConsole` registered

```json
{
  "namespace": "LiteConsole\\",
  "naming exceptions": {
    "new": "NewCommand"
  }
}
```

Naming conventions:

`bin/lite create` command will execute `<AnyNamespace\>LiteConsole\Create` class

`new` word is already reserved in PHP, no option to declare `class New`

In this very case you can specify such an exception inside `naming exceptions` section


# Configure composer

`NB!!!` No need for that if directory with LiteConsole command files is inside `src`

Inside your project's `composer.json`

```json
{
  "autoload": {
    "psr-4": {
      "LiteConsole\\": "LiteConsole/"
    }
  }
}
```

`composer dump-autoload`


# Create Command class file

`/LiteConsole/NewCommand.php` || `/src/LiteConsole/NewCommand.php`

```php
<?php

namespace LiteConsole; // || namespace App\LiteConsole;

use Infotechnohelp\LiteConsole\ConsoleCommand;

class NewCommand extends ConsoleCommand
{
    public function exec()
    {
        $this->getArguments(); // array ['file']
        $this->getArgument(0); // string 'file' 
        $this->getOptions(); // array ['ext' => 'txt']
        $this->optionIsSet('ext'); // bool true
        $this->getOptionValue('ext'); // string 'txt'
        
        // Any execution logics here
    }
}
```

Now you can use `bin/lite new file --ext=txt`