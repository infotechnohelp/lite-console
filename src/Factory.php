<?php

namespace Infotechnohelp\LiteConsole;

class Factory
{
    private $argv;

    private $consoleCommandTitle;

    private $exceptions;

    private $namespace;

    public function __construct(array $argv)
    {
        $this->argv = $argv;
        $this->consoleCommandTitle = $argv[1] ?? null;

        if ($this->consoleCommandTitle === null) {
            echo "No command title specified\n";
            exit;
        }


        // @todo Use
        // Files::json(new File('lite-console'))->get(); Files::json(new File('lite-console'))->put([1,2,3])
        // //$file = new File('lite-console');
        // get(bool $associative = true)
        // //$configJson = Files::json($file);
        // $config = Files::json('console-lite')->get();
        // $configJson->get(); $configJson->put(['colors' => ['warning' => 'red']]); $configJson->merge(['colors' => ['warning' => 'purple']]);
        // $configJson->append()
        $config = json_decode(
            file_get_contents(dirname(__DIR__, 4) . DIRECTORY_SEPARATOR . 'lite-console.json'),
            true
        );
        // @todo $config = Files::json('console-lite')->get();

        $this->namespace = $config['namespace'];
        $this->exceptions = $config['naming exceptions'] ?? [];
    }

    private function prepareConsoleCommandTitle($consoleCommandTitle)
    {
        if(!str_contains($consoleCommandTitle, DIRECTORY_SEPARATOR)){
            return $this->namespace . ucfirst($consoleCommandTitle);
        }

        $exploded = explode(DIRECTORY_SEPARATOR, $consoleCommandTitle);

        $exploded = array_map(function($value){
            return ucfirst($value);
        }, $exploded);

        return $this->namespace . implode("\\", $exploded);
    }

    public function getConsoleCommand(): ?ConsoleCommand
    {
        $consoleCommandClassTitle = null;

        if ($this->consoleCommandTitle === '-h') {
            return new HelpCommand();
        }

        if (array_key_exists($this->consoleCommandTitle, $this->exceptions)) {
            $consoleCommandClassTitle = $this->namespace . $this->exceptions[$this->consoleCommandTitle];
        }

        $consoleCommandClassTitle = $consoleCommandClassTitle ??  $this->prepareConsoleCommandTitle($this->consoleCommandTitle);

        try {
            $commandObject = new $consoleCommandClassTitle($this->argv);
        } catch (\Exception|\Error $e) {
            echo $e->getMessage() . "\n";
            exit;
        }

        return $commandObject;
    }
}